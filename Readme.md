# Quick Spell Casting

Cast spells with a single key press, similarly to how it works in Oblivion. Uses "Ready Magic" and quick key binds.

It is recommended to use the [use magic item animations](https://openmw.readthedocs.io/en/latest/reference/modding/settings/game.html#use-magic-item-animations) setting, but the mod works without it too.

## Requirements

Animation bindings MR https://gitlab.com/OpenMW/openmw/-/merge_requests/3257
